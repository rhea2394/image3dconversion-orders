<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Doc\CCavenueController;
use App\Http\Controllers\Doc\OrdersController;
use App\Order;
use Log;

class PaymentController extends Controller
{
    public function OnlinePayment(Request $request)
    {
        // For Other than Default Gateway
        $response = (new CCavenueController)->Response($request);
        
        $failure_message = isset($response['failure_message']) ? $response['failure_message'] : '';
        
        $paymentgateway_response = serialize($response);

        $order = Order::whereId($response['order_id'])
                        ->first();
        
        if(!$order) {
          return redirect('doc/paymentorder/'.$response['order_id'])->with(["msg" => ["Payment unsuccessful. Please try again."]]);
        }
        
        if ($response['order_status']=='Success') {
            $paymentStatus = 'success';
        } elseif ($response['order_status']=='Failure') {
            $paymentStatus = 'failure';
        } elseif ($response['order_status']=='Aborted') {
            $paymentStatus = 'aborted';
        } elseif ($response['order_status']=='Invalid') {
            $paymentStatus = 'invalid';
        } else {
            $paymentStatus = 'undefined';
        }
        
        /*if($paymentStatus == 'success') {
          $request->session()->forget('orders');
        }*/
        
        return (new OrdersController)->PaymentComplete($order->id, $paymentStatus, $paymentgateway_response, $failure_message);
    }
    
    /*public function statusCheck($order_id)
    {
        $response = PaytmClass::statusCheck($order_id);
        
        $paymentStatus = 'pending';
        if ($response['STATUS'] == 'TXN_SUCCESS') {
          //Transaction Successful
            $paymentStatus = 'success';
        } else if ($response['STATUS'] == 'TXN_FAILURE') {
          //Transaction Failed
            $paymentStatus = 'failure';
        } else if ($response['STATUS'] == 'OPEN') {
          //Transaction Open/Processing
            $paymentStatus = 'open';
        }
        
        return $response;
    }
    
    public function OnlinePaymentManualRetry($order_id)
    {
        $paymentStatus = 'success';
        
        $paymentgateway_response = '';
        
        $failure_message = '';
        
        // return (new CheckoutController)->PaymentComplete($order_id, $paymentStatus, $paymentgateway_response, $failure_message);
    }*/
}
