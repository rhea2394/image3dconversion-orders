<?php
use App\BoneSegmentationCost;
use App\TreatmentPlanningDesignCost;
use App\TreatmentPlanningDesignFabricationCost;
use App\DigitalDentureCost;
use App\OrthodonticTreatmentCost;
use App\RadiologyReportCost;
use App\MetalComponentsCost;
use App\ZygomaGuidedSurgeryCost;
use App\ExchangeRate;

function showServicesFromArray($services) {
    $services = json_decode($services,true);
    $temp_service_key = [];
    foreach($services as $key=>$service) {
        $temp_service_key[] = ucwords(str_replace("_", " ", $key));
    }
    return implode(', ', $temp_service_key);
}

function styleString($string) {
    return ucwords(str_replace("_"," ",$string));
}

function firstName($string) {
    $arr = explode(' ', $string);
    $num = count($arr);
    
    $first_name = '';
    //$middle_name = '';
    $last_name = '';
    
    if ($num == 1) {
        return $string;
    }
    
    $last_name = $arr[$num-1];
    
    for($i=0; $i<($num-1); $i++) {
        $first_name = $first_name.$arr[$i].' ';
    }
    
    return trim($first_name);
}

function array_flatten($array) { 
  if (!is_array($array)) { 
    return FALSE; 
  } 
  $result = array(); 
  foreach ($array as $key => $value) { 
    if (is_array($value)) { 
      $result = array_merge($result, array_flatten($value)); 
    } 
    else { 
      $result[$key] = $value; 
    } 
  } 
  return $result; 
}

function invoice($user_id, $order_id){
	/*$date = date('s', time());*/
	$temp = $user_id.$order_id;
	return ((int)$temp + 1000);
}

function now_date() {
  return (date("d-M-Y"));
}

function ConvertDate($sql_date) {
	$date=strtotime($sql_date);
	$final_date=strftime("%d/%m/%Y", $date);
	return $final_date;
}

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function service_cost($service,$country) 
{
    if($country != 'india') {
        $country = 'others';
    }
    
    if($service == 'bone_segmentation') {
        return BoneSegmentationCost::firstOrNew(['country' => $country]);
    }
    elseif($service == 'treatment_planning_design') {
        return TreatmentPlanningDesignCost::firstOrNew(['country' => $country]);
    }
    elseif($service == 'treatment_planning_design_fabrication') {
        return TreatmentPlanningDesignFabricationCost::firstOrNew(['country' => $country]);
    }
    elseif($service == 'digital_denture') {
        return DigitalDentureCost::firstOrNew(['country' => $country]);
    }
    elseif($service == 'orthodontic_treatment') {
        return OrthodonticTreatmentCost::firstOrNew(['country' => $country]);
    }
    elseif($service == 'radiology_report') {
        return RadiologyReportCost::firstOrNew(['country' => $country]);
    }
    elseif($service == 'metal_components') {
        return MetalComponentsCost::firstOrNew(['country' => $country]);
    }
    elseif($service == 'zygoma_guided_surgery') {
        return ZygomaGuidedSurgeryCost::firstOrNew(['country' => $country]);
    }
}

function toINR($usd_amount,$exchange_rate=null) {
    if($exchange_rate) {
        return round(bcdiv(($usd_amount*$exchange_rate), 1, 2),0);
        // return round($usd_amount*$exchange_rate,2);
    } else {
        return round(bcdiv(($usd_amount*get_exchange_rate()), 1, 2),0);
        // return round($usd_amount*get_exchange_rate(),2);
    }
}

function toUSD($inr_amount) {
    return $inr_amount/get_exchange_rate();
}

function get_exchange_rate() {
    $exchange_rate = ExchangeRate::find(1);
    return $exchange_rate->exchange_rate;
}

function display_currency($usd_amount, $opt_country = null, $mail_flag = false, $exchange_rate = null) {
    $country = $opt_country ? $opt_country : session('country', 'others');
    
    if($usd_amount == 0) {
        if($country == 'india') {
            if($mail_flag) {
                return 'Rs. 0';
            } else {
                return '<i class="fas fa-rupee-sign"></i> 0';
            }
        } else {
            if($mail_flag) {
                return '$ 0';
            } else {
                return '<i class="fas fa-dollar-sign"></i> 0';
            }
        }
    }
    
    if($country == 'india') {
        if($mail_flag) {
            return 'Rs. '.toINR($usd_amount,$exchange_rate);
        } else {
            return '<i class="fas fa-rupee-sign"></i> '.toINR($usd_amount,$exchange_rate);
        }
    } else {
        if($mail_flag) {
            return '$ '.$usd_amount;
        } else {
            return '<i class="fas fa-dollar-sign"></i> '.$usd_amount;
        }
        
    }
}

function display_currency_symbol($opt_country) {
    $country = $opt_country ? $opt_country : session('country', 'others');
    
        if($country == 'india') {
            return '<i class="fas fa-rupee-sign"></i>';
        } else {
            return '<i class="fas fa-dollar-sign"></i>';
        }
}

function autoConvert($amount,$country) {
    if($country == 'india') {
        return toUSD($amount);
    } else {
        return $amount;
    }
}

function autoConvertFrontend($amount,$country) {
    if($country == 'india') {
        return toINR($amount);
    } else {
        return $amount;
    }
}

function generateTxnId($id = ''){
	return (int)substr($id.mt_rand() . microtime(), 0, 17);
}

/*function array_round_fn($n){
return(round($n,0));
}

function array_roundoff($a) {
    return array_map("array_round_fn",$a);
}*/

function implodeArrayKeys($array) {
    return implode(", ",array_keys($array));
}

function ArrayImplodeFn($value, $convertToSentence = null) //can be array or string
{
    if(is_array($value)) {
        if($convertToSentence = true) {
            $string = implode(', ', $value);
            return str_replace('_',' ',ucwords($string,', '));
        }
        return implode(', ', $value);
    }
    
    return $value;
}

function getCountry() {
    /*$fetch_geoip = json_decode(file_get_contents('http://free.ipwhois.io/json/'.$_SERVER['REMOTE_ADDR']));
    if($fetch_geoip && ($fetch_geoip->success == true)) {
        $country = strtolower($fetch_geoip->country);
        return $country;
    }
    else {
        return dd("Unable to Determine Country");
    }*/
    
    /*------------Maxmind Direct----------*/
    //use GeoIp2\Database\Reader;
    
    /*$reader = new Reader(storage_path('app/geoip2/GeoLite2-Country.mmdb'));
    $record = $reader->country($_SERVER['REMOTE_ADDR']);
    if($record) {
        if(isset($record->country->name)) {
            $country = strtolower($record->country->name);
            return $country;
        }
    }
    
    return dd("Unable to Determine Country");*/
    
    if(geoip()->getLocation($_SERVER['REMOTE_ADDR'])) {
        $country = geoip()->getLocation($_SERVER['REMOTE_ADDR'])['country']; 
        return strtolower($country);
    }
    
    return dd("Unable to Determine Country");
}

function roundAmount($total) {
    return round($total,2);
}