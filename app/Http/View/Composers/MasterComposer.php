<?php
namespace App\Http\View\Composers;

use Illuminate\View\View;

class MasterComposer
{
    /**
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $phase = ["Not Processed","Process","Ongoing Order (Treatment Planning)","Fabrication","Out for Delivery","Phase 5","Phase 6","Phase 7","Phase 8","Phase 9","Phase 10"];
        
        $data = [
            'phase' => $phase,
            ];
            
        $view->with($data);
    }
}