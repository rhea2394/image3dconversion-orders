<!DOCTYPE html>
<html>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T8BNCHV');</script>
<!-- End Google Tag Manager -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Image3dconversion | Admin Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/vendor/adminlte/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="/vendor/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/vendor/adminlte/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style type="text/css">
    .login-page, .register-page {
      background: #ffffff;
    }
    .login-logo a, .register-logo a {
        color: #111111;
    }
    .footer {
      position: fixed;
      left: 0;
      bottom: 0;
      width: 100%;
      text-align: center;
      background: #3d5b9b;
      color: #ffffff;
      padding: 10px;
    }
    .footer a{
      color: #fff;
    }
  </style>
  <link rel="stylesheet" href="/storage/css/sagar-admin.css?t={{time()}}">
</head>
<body class="hold-transition login-page">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T8BNCHV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="login-box">
  <div class="login-logo">
    <img src="{{asset('/images/i3dlogo.png')}}" style="max-width:200px;background:#fff;max-height: 40px;">
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">

      <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="input-group mb-3">
          <input placeholder="E-mail" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Login') }}
                                </button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <!--<p class="mb-1">
        @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
      </p>-->
      <!--<p class="mb-0">
        <a class="nav-link" class="text-center" href="/doc">Goto Doctor Login</a>
      </p>-->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- Main Footer -->
  <div class="footer">
    <div class="float-left">Copyright &copy;2019 Image3d Conversion. All rights reserved.</div>
    <div class="float-right">
      Toll Free No: <img style="max-width:15px" src="/images/us.png" alt=""> &nbsp;USA & Canada: <a href="tel:+18882769811">(+1)888-276-9811</a> <img style="max-width:15px" src="/images/uk.png" alt=""> &nbsp;UK: <a href="tel:+448000903841">(+44)800-090-3841</a> <i class="fab fa-whatsapp"></i> &nbsp;<a rel="noopener" target="_blank" title="Chat on Whatsapp" href="https://api.whatsapp.com/send?phone=918876395581">(+91)8876395581</a> <i class="far fa-envelope"></i> &nbsp;<a href="mailto:info@image3dconversion.com">info@image3dconversion.com</a>
    </div>
    <div class="clearfix"></div>
  </div>
  
<!-- jQuery -->
<script src="/vendor/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/vendor/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/vendor/adminlte/js/adminlte.min.js"></script>

</body>
</html>
