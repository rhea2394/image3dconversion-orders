@extends('layouts.master')

@section('page-title', 'New Coupon')

@section('style')
	@parent
<link rel="stylesheet" href="/css/flatpickr.min.css">
<style type="text/css">
	.flatpickr-input{
		background-color: #ffffff!important;
	}
</style>
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/coupons">Coupons</a></li>
    <li class="breadcrumb-item active">New</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
            <div class="col-md-10">
            <div class="card">
                <form class="form-horizontal" role="form" action="/coupons" method="post" enctype="multipart/form-data">
                    @csrf
                <div class="card-body">
                		<div class="form-group row">
							<label class="col-sm-3 control-label">Country:</label>
							<div class="col-sm-3">
							<select name="country" class="form-control">
							    <option value="india">India</option>
							    <option value="others">Others</option>
							</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">Coupon Code:*</label>
							<div class="col-sm-9">
							<div class="input-group">
						      <input id="couponCode" type="text" class="form-control" name="code" value="{{ old('code') }}" required placeholder="code">
						      <span class="input-group-btn">
						        <button id="codeGen" class="btn btn-default" type="button">Generate</button>
						      </span>
						    </div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">Description:</label>
							<div class="col-sm-9">
							<textarea class="form-control" name="description" rows="3" placeholder="Full description for user readability">{!! old('description') !!}</textarea>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">Validity From:*</label>
							<div class="col-sm-9">
							<input id="valid_from" type="text" class="form-control" name="valid_from" value="{{ old('valid_from') }}" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">Validity To:*</label>
							<div class="col-sm-9">
							<input id="valid_upto" type="text" class="form-control" name="valid_upto" value="{{ old('valid_upto') }}" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">Amount Range ($):</label>
							<div class="col-sm-9 row">
								<div class="col-md-6"><input type="text" class="form-control" name="price_min" placeholder="Minimum" value="{{ old('price_min') }}"></div>
								<div class="col-md-6"><input type="text" class="form-control" name="price_max" placeholder="Maximum" value="{{ old('price_max') }}"></div>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">Discount Type:</label>
						    <div class="col-sm-9">
                                <label class="radio-inline">
								  <input type="radio" name="type" id="inlineRadio1" value="1" @if(old('type') == 1 || old('type') == "") checked="checked" @endif> Percentage
								</label>
								<label class="radio-inline">
								  <input type="radio" name="type" id="inlineRadio2" value="2" @if(old('type') == 2) checked="checked" @endif> Flat
								</label>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">Discount ($):*</label>
							<div class="col-sm-9">
							<input type="number" class="form-control" name="discount" value="{{ old('discount') }}" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">Coupons Quantity:*</label>
							<div class="col-sm-9">
							<input type="number" class="form-control" name="nos" value="{{ old('nos') }}" required>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">No of times per user:*</label>
							<div class="col-sm-9">
							<input type="number" class="form-control" name="nos_per_user" value="{{ old('nos_per_user') }}">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-sm-3 control-label">Select Service:</label>
							<div class="col-sm-9">
							<select class="form-control" name="service">
								<option>All</option>
								<option value="bone_segmentation" @if(old('service') == 'bone_segmentation') selected="selected" @endif>Bone Segmentation</option>
								<option value="treatment_planning_design" @if(old('service') == 'treatment_planning_design') selected="selected" @endif>Treatment Planning Design</option>
								<option value="treatment_planning_design_fabrication" @if(old('service') == 'treatment_planning_design_fabrication') selected="selected" @endif>Treatment Planning Design Fabrication</option>
								<option value="digital_denture" @if(old('service') == 'digital_denture') selected="selected" @endif>Digital Denture</option>
								<option value="orthodontic_treatment" @if(old('service') == 'orthodontic_treatment') selected="selected" @endif>Orthodontic Treatment</option>
								<option value="radiology_report" @if(old('service') == 'radiology_report') selected="selected" @endif>Radiology Report</option>
								<option value="metal_components" @if(old('service') == 'metal_components') selected="selected" @endif>Metal Components</option>
								<option value="zygoma_guided_surgery" @if(old('service') == 'zygoma_guided_surgery') selected="selected" @endif>Zygoma Guided Surgery</option>
							</select>
							</div>
						</div>
						<div class="form-group row">
						    <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
								<label>
								    <input type="checkbox" name="first_time_flag" value="1" @if(old('first_time_flag')) checked="checked" @endif>First Time User
								</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
						    <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
								<label>
								    <input type="checkbox" name="deactivate" value="1" @if(old('deactivate')) checked="checked" @endif>Deactivate
								</label>
								</div>
							</div>
						</div>
                </div>
                <div class="card-footer">
                    <button name="action" type="submit" value="SUBMIT" class="btn btn-primary">Submit</button>
                </div>
                </form>
            </div>
            </div>
            
        </div>
    </section>
@endsection

@section('script')
	@parent
<script src="/js/flatpickr.js"></script>
<script type="text/javascript">
	function password(length, special) {
	  var iteration = 0;
	  var password = "";
	  var randomNumber;
	  if(special == undefined){
		  var special = false;
	  }
	  while(iteration < length){
		randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
		if(!special){
		  if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
		  if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
		  if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
		  if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
		}
		iteration++;
		password += String.fromCharCode(randomNumber);
	  }
	  return password.toUpperCase();
	}
	$(document).ready( function() {
		$("#codeGen").on('click', function(event) {
			var code=password(6);
			$('#couponCode').val(code);
		});
		
		$("#valid_from").flatpickr({
		    enableTime: true,
		    dateFormat: "Y-m-d H:i",
		});
		
		$("#valid_upto").flatpickr({
		    enableTime: true,
		    dateFormat: "Y-m-d H:i",
		});
	});
 </script>
@endsection