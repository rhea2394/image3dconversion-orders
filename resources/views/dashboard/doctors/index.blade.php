@extends('layouts.master')

@section('page-title', 'Doctors')

@section('style')
  @parent
<!-- DataTables -->
  <link rel="stylesheet" href="/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <style type="text/css">
    .products-list .product-info {
        margin-left: 0;
    }
  </style>
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active">Doctors</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
              <table id="doctorsTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Contact</th>
                  <th>Email</th>
                  <th>Country</th>
                  <th>Total Orders</th>
                  <th>Amount</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($doctors as $doctor)
                <tr>
                  <td>{{$doctor->name}}</td>
                  <td>{{$doctor->mobile}}</td>
                  <td>{{$doctor->email}}</td>
                  <td>-</td>
                  <td>{{$doctor->total_orders}}</td>
                  <td>{{$doctor->total_amount}}</td>
                  <td>
                    <a href="/doctor/{{$doctor->id}}" class="btn btn-info btn-sm">Details</a>
                  </td>
                </tr>
                @endforeach
                </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          
          <div class="row">
          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Top 5 Doctors</h3>

                <!--<div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <ul class="products-list product-list-in-card pl-2 pr-2">
                  @foreach($topDocs as $key=>$docs)
                  <li class="item">
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title text-capitalize">{{$key}}
                        <span class="badge badge-success float-right">${{$docs}}</span></a>
                    </div>
                  </li>
                  @endforeach
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
        </div>
        
        <div class="col-md-4">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Top 5 Countries</h3>

                <!--<div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <ul class="products-list product-list-in-card pl-2 pr-2">
                  @foreach($topCountries as $key=>$value)
                  <li class="item">
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title text-capitalize">{{$key}}
                        <span class="badge badge-success float-right">${{$value}}</span></a>
                    </div>
                  </li>
                  @endforeach
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          </div>
          

        </div>
    </section>
@endsection

@section('script')
  @parent
<script src="/vendor/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
$("#doctorsTable").DataTable({
    	"aaSorting": [],
        "columnDefs": [
            {
            "targets": 6,
            "orderable": false
            },
            {
            "targets": 5,
		        render: function ( data, type, row, meta ) {
		        	return '$' + data;
		        }
            }
            ]
    });
</script>
@endsection