@extends('layouts.master')

@section('page-title', 'Edit Cost')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/cost">Service Cost</a></li>
    <li class="breadcrumb-item active">Edit Cost</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        	<div class="col-md-9">
            <form class="form-horizontal" role="form" action="/cost/update" method="post" enctype="multipart/form-data">
                @csrf
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
						<div class="form-group row">
							<label class="col-sm-3 control-label">Country:</label>
							<div class="col-sm-9">
							<select name="" class="form-control" disabled="disabled">
							    <option value="india" {{($country=='india') ? 'selected="selected"' : ''}}>India</option>
							    <option value="others" {{($country=='others') ? 'selected="selected"' : ''}}>Others</option>
							</select>
							<input type="hidden" name="country" value="{{$country}}"/>
							</div>
						</div>
				</div>
            </div>
            
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Bone Segmentation ({!!display_currency_symbol($country)!!})
                </div>
                <div class="card-body">
					<div class="form-group row">
						<label class="col-sm-3 control-label">Basic Conversion:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="bone_segmentation_cost[basic_conversion]" value="{{ isset($bone_segmentation_cost['basic_conversion']) ? autoConvertFrontend($bone_segmentation_cost['basic_conversion'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Virtual Extraction:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="bone_segmentation_cost[virtual_extraction]" value="{{ isset($bone_segmentation_cost['virtual_extraction']) ? autoConvertFrontend($bone_segmentation_cost['virtual_extraction'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Zygomatic Conversion:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="bone_segmentation_cost[zygomatic_conversion]" value="{{ isset($bone_segmentation_cost['zygomatic_conversion']) ? autoConvertFrontend($bone_segmentation_cost['zygomatic_conversion'],$country) : '' }}"/>
						</div>
					</div>
				</div>
            </div>
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Treatment Planning Design ({!!display_currency_symbol($country)!!})
                </div>
                <div class="card-body">
					<div class="form-group row">
						<label class="col-sm-3 control-label">Bone Reduction Case:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_cost[bone_reduction_case]" value="{{ isset($treatment_planning_design_cost['bone_reduction_case']) ? autoConvertFrontend($treatment_planning_design_cost['bone_reduction_case'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Tooth Supported Case:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_cost[tooth_supported_case]" value="{{ isset($treatment_planning_design_cost['tooth_supported_case']) ? autoConvertFrontend($treatment_planning_design_cost['tooth_supported_case'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Tooth Supported Case Additional:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_cost[tooth_supported_case_additional]" value="{{ isset($treatment_planning_design_cost['tooth_supported_case_additional']) ? autoConvertFrontend($treatment_planning_design_cost['tooth_supported_case_additional'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Tissue Supported Case:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_cost[tissue_supported_case]" value="{{ isset($treatment_planning_design_cost['tissue_supported_case']) ? autoConvertFrontend($treatment_planning_design_cost['tissue_supported_case'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Bone Supported Case:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_cost[bone_supported_case]" value="{{ isset($treatment_planning_design_cost['bone_supported_case']) ? autoConvertFrontend($treatment_planning_design_cost['bone_supported_case'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Provisional Planning (Single Arch):</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_cost[provisional_single_arch]" value="{{ isset($treatment_planning_design_cost['provisional_single_arch']) ? autoConvertFrontend($treatment_planning_design_cost['provisional_single_arch'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Provisional Planning (Double Arch):</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_cost[provisional_double_arch]" value="{{ isset($treatment_planning_design_cost['provisional_double_arch']) ? autoConvertFrontend($treatment_planning_design_cost['provisional_double_arch'],$country) : '' }}"/>
						</div>
					</div>
				</div>
            </div>
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Treatment Planning Design Fabrication ({!!display_currency_symbol($country)!!})
                </div>
                <div class="card-body">
					<div class="form-group row">
						<label class="col-sm-3 control-label">Bone Reduction Case:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_fabrication_cost[bone_reduction_case]" value="{{ isset($treatment_planning_design_fabrication_cost['bone_reduction_case']) ? autoConvertFrontend($treatment_planning_design_fabrication_cost['bone_reduction_case'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Tooth Supported Case:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_fabrication_cost[tooth_supported_case]" value="{{ isset($treatment_planning_design_fabrication_cost['tooth_supported_case']) ? autoConvertFrontend($treatment_planning_design_fabrication_cost['tooth_supported_case'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Tooth Supported Case Additional:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_fabrication_cost[tooth_supported_case_additional]" value="{{ isset($treatment_planning_design_fabrication_cost['tooth_supported_case_additional']) ? autoConvertFrontend($treatment_planning_design_fabrication_cost['tooth_supported_case_additional'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Tissue Supported Case:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_fabrication_cost[tissue_supported_case]" value="{{ isset($treatment_planning_design_fabrication_cost['tissue_supported_case']) ? autoConvertFrontend($treatment_planning_design_fabrication_cost['tissue_supported_case'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Bone Supported Case:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_fabrication_cost[bone_supported_case]" value="{{ isset($treatment_planning_design_fabrication_cost['bone_supported_case']) ? autoConvertFrontend($treatment_planning_design_fabrication_cost['bone_supported_case'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Provisional Planning (Single Arch):</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_fabrication_cost[provisional_single_arch]" value="{{ isset($treatment_planning_design_fabrication_cost['provisional_single_arch']) ? autoConvertFrontend($treatment_planning_design_fabrication_cost['provisional_single_arch'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Provisional Planning (Double Arch):</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="treatment_planning_design_fabrication_cost[provisional_double_arch]" value="{{ isset($treatment_planning_design_fabrication_cost['provisional_double_arch']) ? autoConvertFrontend($treatment_planning_design_fabrication_cost['provisional_double_arch'],$country) : '' }}"/>
						</div>
					</div>
				</div>
            </div>
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Digital Denture ({!!display_currency_symbol($country)!!})
                </div>
                <div class="card-body">
					<div class="form-group row">
						<label class="col-sm-3 control-label">Planning:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="digital_denture_cost[print]" value="{{ isset($digital_denture_cost['print']) ? autoConvertFrontend($digital_denture_cost['print'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Planning &amp; Printing:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="digital_denture_cost[printing_planning]" value="{{ isset($digital_denture_cost['printing_planning']) ? autoConvertFrontend($digital_denture_cost['printing_planning'],$country) : '' }}"/>
						</div>
					</div>
				</div>
            </div>
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Orthodontic Treatment Cost ({!!display_currency_symbol($country)!!})
                </div>
                <div class="card-body">
					<div class="form-group row">
						<label class="col-sm-3 control-label">Ortho Planning:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="orthodontic_treatment_cost[ortho_planning]" value="{{ isset($orthodontic_treatment_cost['ortho_planning']) ? autoConvertFrontend($orthodontic_treatment_cost['ortho_planning'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Ortho Model Print:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="orthodontic_treatment_cost[ortho_model_print]" value="{{ isset($orthodontic_treatment_cost['ortho_model_print']) ? autoConvertFrontend($orthodontic_treatment_cost['ortho_model_print'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Tray Fabrication:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="orthodontic_treatment_cost[tray_fabrication]" value="{{ isset($orthodontic_treatment_cost['tray_fabrication']) ? autoConvertFrontend($orthodontic_treatment_cost['tray_fabrication'],$country) : '' }}"/>
						</div>
					</div>
				</div>
            </div>
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Radiology Report Cost ({!!display_currency_symbol($country)!!})
                </div>
                <div class="card-body">
					<div class="form-group row">
						<label class="col-sm-3 control-label">Flat Rate:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="radiology_report_cost[flat_rate]" value="{{ isset($radiology_report_cost['flat_rate']) ? autoConvertFrontend($radiology_report_cost['flat_rate'],$country) : '' }}"/>
						</div>
					</div>
				</div>
            </div>
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Metal Components Cost ({!!display_currency_symbol($country)!!})
                </div>
                <div class="card-body">
					<div class="form-group row">
						<label class="col-sm-3 control-label">Guide Tubes:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="metal_components_cost[guide_tubes]" value="{{ isset($metal_components_cost['guide_tubes']) ? autoConvertFrontend($metal_components_cost['guide_tubes'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Fixation Pins:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="metal_components_cost[fixation_pins]" value="{{ isset($metal_components_cost['fixation_pins']) ? autoConvertFrontend($metal_components_cost['fixation_pins'],$country) : '' }}"/>
						</div>
					</div>
				</div>
            </div>
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Zygoma Guided Surgery Cost ({!!display_currency_symbol($country)!!})
                </div>
                <div class="card-body">
					<div class="form-group row">
						<label class="col-sm-3 control-label">Quadrant Pterygoid:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="zygoma_guided_surgery_cost[quadrant_pterygoid]" value="{{ isset($zygoma_guided_surgery_cost['quadrant_pterygoid']) ? autoConvertFrontend($zygoma_guided_surgery_cost['quadrant_pterygoid'],$country) : '' }}"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 control-label">Full Pterygoid:</label>
						<div class="col-sm-9">
						<input class="form-control" type="text" name="zygoma_guided_surgery_cost[full_pterygoid]" value="{{ isset($zygoma_guided_surgery_cost['full_pterygoid']) ? autoConvertFrontend($zygoma_guided_surgery_cost['full_pterygoid'],$country) : '' }}"/>
						</div>
					</div>
				</div>
            </div>
              <hr/>
              <button name="action" type="submit" value="SUBMIT" class="btn btn-primary">Submit</button>
            </form>
              <!-- /.card-footer -->
            </div>
            </div>
        </div>
    </section>
@endsection