<html>
    <head>
        <title>Dropbox Test</title>
    </head>
    <body>
        <form>
          <input type="file" id="file-upload" />
          <br/>
          <button id="uploadBtn" type="button">Submit</button>
        </form>
    
        <!-- A place to show the status of the upload -->
        <h2 id="results"></h2>
        <script type="text/javascript">
            const order_id = 66;
        </script>
        <script type="text/javascript" src="{{mix('js/dropbox.js')}}"></script>
    </body>
</html>