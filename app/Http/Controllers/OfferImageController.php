<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OfferImage;
use Storage;
use Log;

class OfferImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offerimages = OfferImage::all();
        $data = ['offerimages' => $offerimages];
        return view('dashboard.offerimage.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.offerimage.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'image' => 'required|file',
		]);
		
		    $file = $request->file('image');
		    
		    $path = $file->getRealPath();
            $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = strtolower($file->getClientOriginalExtension()); // getting image extension
            $filesize = $file->getSize();
            $filesize = $filesize/1000000; /*-----MB------*/
            $fileName = 'offer_'.clean($originalName).'_'.time().'.'.$extension; // renaming image
        
            /*---------check filetype----------*/
            if (!($extension==('jpg'||'jpeg'||'png'||'gif'))) {
                $msg = ['Please provide image in the following formats (jpg, png, gif)'];
                return redirect()->back()->with(['msg' => $msg]);
            }
            /*--------check filesize------------*/
            if ($filesize>3) {    /*---------3MB-------*/
                $msg = ['Please provide an image of size less than 3MB'];
                return redirect()->back()->with(['msg' => $msg]);
            }
            
            $offerimage = new OfferImage;
            $offerimage->name = $request->input('name');
            $offerimage->description = $request->input('description');
            $offerimage->save();
            
            $offer_image_path = 'offers/'.$offerimage->id.'/';
            Storage::disk('public')->put($offer_image_path.$fileName, file_get_contents($path));
            $offerimage->location = $offer_image_path.$fileName;
            $offerimage->save();
            
            $data = ["msg" => ["Offer image added successfully"]];
		    return redirect('/offerimage')->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OfferImage::destroy($id);
		return response()->json([
            'msg' => "Offer image deleted",
        ], 200);
    }
}
