@extends('doclayouts.master')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/doc">Home</a></li>
    <li class="breadcrumb-item active">Step-3</li>
</ol>
@endsection

@section('style')
  @parent
  <link rel="stylesheet" href="/vendor/jquery-confirm/jquery-confirm.min.css">
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="col-md-10 mx-auto">
                <center>
              <h1 style="font-size: 25px;">Step-3</h1>
              <h2 class="text-theme-colored mt-0" style="color: #3d5b9b !important;margin-bottom: 10px;font-family: 'Droid Serif', sans-serif;
    font-weight: 700;
    line-height: 1.42857143;font-size: 20px;display: block;">Preview Your Order
</h2>
<!--<h3 class="text-gray mt-0 mt-sm-30 mb-0" style="margin-top: 30px !;    color: #808080;font-family: 'Droid Serif', sans-serif;
    font-weight: 700;font-size: 18px;">Please fill up the Information below for your case.
</h3>-->

</center>
        <form method="post" action="/doc/submitorder">
                @csrf
            <input name="alt_address" type="hidden" value="{{$alt_address}}">
            <input name="patient_name" type="hidden" value="{{$patient_name}}">
            <input name="surgery_date" type="hidden" value="{{$surgery_date}}">
            <input name="services" type="hidden" value="{{json_encode($services,true)}}">
            <input type="hidden" name="country" value="{{ $country }}"/>
            <div class="card">
        <div class="card-header bg-primary text-white">
                Doctor Information
        </div>
        <div class="card-body">
            <dl class="row">
              <dt class="col-sm-3">Doctor's Name</dt>
              <dd class="col-sm-9">{{$user->name}}</dd>
              
              <dt class="col-sm-3">Email</dt>
              <dd class="col-sm-9">{{$user->email}}</dd>
              
              <dt class="col-sm-3">Contact Number</dt>
              <dd class="col-sm-9">{{$user->mobile}}</dd>
              
              <dt class="col-sm-3">Address</dt>
              <dd class="col-sm-9">{{$user->address}}</dd>
              
              <dt class="col-sm-3">Shipping Address</dt>
              <dd class="col-sm-9">{{$alt_address}}</dd>
            </dl>
        </div>
    </div>
    <div class="card">
            <div class="card-header bg-primary text-white">
                    Patient Information
            </div>
            <div class="card-body">
                <dl class="row">
                  <dt class="col-sm-3">Patient Name</dt>
                  <dd class="col-sm-9">{{$patient_name}}</dd>
                  
                  <dt class="col-sm-3">Surgery Date</dt>
                  <dd class="col-sm-9">{{$surgery_date}}</dd>
                </dl>
            </div>
    </div>
<?php
    $total_cost = 0;
    $bone_segmentation_subtotal = 0;
    $treatment_planning_design_subtotal = 0;
    $treatment_planning_design_fabrication_subtotal = 0;
    $digital_denture_subtotal = 0;
    $orthodontic_treatment_subtotal = 0;
    $radiology_report_subtotal = 0;
    $metal_components_subtotal = 0;
    $zygoma_guided_surgery_subtotal = 0;
?>
    <div class="card">
        <div class="card-header bg-primary text-white">
                Services with Cost
        </div>
        <div class="card-body">
            @foreach($services as $key=>$service)
            
            @if($key == 'bone_segmentation')
                <h4><strong><u>Bone Segmentation</u></strong></h4>
                @foreach($serviceDetails[$key] as $subkey=>$bone_segmentation)
                    @if(isset($bone_segmentation->basic_conversion) && ($bone_segmentation->basic_conversion == 1))
                        Basic Conversion : {!! display_currency($bone_segmentation_cost['basic_conversion']) !!}
                        <?php
                            $total_cost += $bone_segmentation_cost['basic_conversion'];
                            $bone_segmentation_subtotal += $bone_segmentation_cost['basic_conversion'];
                        ?>
                    @endif
                    @if(isset($bone_segmentation->virtual_extraction) && ($bone_segmentation->virtual_extraction == 1))
                        Virtual Extraction : {!! display_currency($bone_segmentation_cost['virtual_extraction']) !!}
                        <?php
                            $total_cost += $bone_segmentation_cost['virtual_extraction'];
                            $bone_segmentation_subtotal += $bone_segmentation_cost['virtual_extraction'];
                        ?>
                    @endif
                    @if(isset($bone_segmentation->zygomatic_conversion) && ($bone_segmentation->zygomatic_conversion == 1))
                        Zygomatic Conversion : {!! display_currency($bone_segmentation_cost['zygomatic_conversion']) !!}
                        <?php
                            $total_cost += $bone_segmentation_cost['zygomatic_conversion'];
                            $bone_segmentation_subtotal += $bone_segmentation_cost['zygomatic_conversion'];
                        ?>
                    @endif
                    @if(isset($bone_segmentation->jaw_information))
                        <br/>
                        Jaw Information : {{$bone_segmentation->jaw_information}}
                    @endif
                    @if(isset($bone_segmentation->order_description))
                        <br/>
                        Order Description : {{$bone_segmentation->order_description}}
                    @endif
                @endforeach
                <!--<p>{{$bone_segmentation_subtotal}}</p>-->
                <input type="hidden" name="bone_segmentation_cost" value="{{json_encode($bone_segmentation_cost, true)}}"/>
                <input name="bone_segmentation_subtotal" type="hidden" value="{{$bone_segmentation_subtotal}}"/>
                <hr/>
            @endif
            
            @if($key == 'treatment_planning_design')
                <h4><strong><u>Treatment Planning (Design Only)</u></strong></h4>
                @foreach($serviceDetails[$key] as $treatment_planning_design)
                    @if(isset($treatment_planning_design->bone_reduction_case) && ($treatment_planning_design->bone_reduction_case == 1))
                        Bone Reduction Case : {!! display_currency($treatment_planning_design_cost['bone_reduction_case']) !!}
                    <?php
                        $total_cost += $treatment_planning_design_cost['bone_reduction_case'];
                        $treatment_planning_design_subtotal += $treatment_planning_design_cost['bone_reduction_case'];
                    ?>
                    @endif
                    @if(isset($treatment_planning_design->tooth_supported_case) && ($treatment_planning_design->tooth_supported_case == 1))
                        <br/>
                        Tooth Supported Case : {!! display_currency($treatment_planning_design_cost['tooth_supported_case']) !!}
                        <?php
                            $total_cost += $treatment_planning_design_cost['tooth_supported_case'];
                            $treatment_planning_design_subtotal += $treatment_planning_design_cost['tooth_supported_case'];
                        ?>
                    @endif
                    @if(isset($treatment_planning_design->tissue_supported_case) && ($treatment_planning_design->tissue_supported_case == 1))
                        <br/>
                        Tissue Supported Case : {!! display_currency($treatment_planning_design_cost['tissue_supported_case']) !!}
                        <?php
                            $total_cost += $treatment_planning_design_cost['tissue_supported_case'];
                            $treatment_planning_design_subtotal += $treatment_planning_design_cost['tissue_supported_case'];
                        ?>
                    @endif
                    @if(isset($treatment_planning_design->bone_supported_case) && ($treatment_planning_design->bone_supported_case == 1))
                        <br/>
                        Bone Supported Case : {!! display_currency($treatment_planning_design_cost['bone_supported_case']) !!}
                        <?php
                            $total_cost += $treatment_planning_design_cost['bone_supported_case'];
                            $treatment_planning_design_subtotal += $treatment_planning_design_cost['bone_supported_case'];
                        ?>
                    @endif
                    @if(isset($treatment_planning_design->provisional_planning) && ($treatment_planning_design->provisional_planning == 1))
                        <br/>
                        Provisional Planning : {!! display_currency($treatment_planning_design_cost['provisional_planning']) !!}
                        <?php
                            $total_cost += $treatment_planning_design_cost['provisional_planning'];
                            $treatment_planning_design_subtotal += $treatment_planning_design_cost['provisional_planning'];
                        ?>
                    @endif
                    
                    @if(isset($treatment_planning_design->tooth_supported_case) && ($treatment_planning_design->no_of_implants > 1))
                        <br/>
                        <?php $temp_calc = $treatment_planning_design_cost['tooth_supported_case_additional'] * (abs($treatment_planning_design->no_of_implants) - 1); ?>
                        Additional Implant Cost : {!! display_currency($treatment_planning_design_cost['tooth_supported_case_additional']) !!} x  {!! display_currency(abs($treatment_planning_design->no_of_implants) - 1) !!} = {!! display_currency($temp_calc) !!}
                        <?php
                            $total_cost += $temp_calc;
                            $treatment_planning_design_subtotal += $temp_calc;
                        ?>
                    @endif
                    
                    @if(isset($treatment_planning_design->preferred_implant))
                        <br/>
                        Preferred Implant : {{$treatment_planning_design->preferred_implant}}
                    @endif
                    @if(isset($treatment_planning_design->jaw_information))
                        <br/>
                        Jaw Information : {{$treatment_planning_design->jaw_information}}
                    @endif
                    @if(isset($treatment_planning_design->restorative_plan))
                        <br/>
                        Restorative Plan : {{$treatment_planning_design->restorative_plan}}
                    @endif
                    @if(isset($treatment_planning_design->surgery_date))
                        <br/>
                        Surgery Date : {{$treatment_planning_design->surgery_date}}
                    @endif
                    @if(isset($treatment_planning_design->provisional_planning_reqd))
                        <br/>
                        Provisional Planning Reqd : {{$treatment_planning_design->provisional_planning_reqd}}
                    @endif
                @endforeach
                <!--<p>{{$treatment_planning_design_subtotal}}</p>-->
                <input type="hidden" name="treatment_planning_design_cost" value="{{json_encode($treatment_planning_design_cost, true)}}"/>
                <input name="treatment_planning_design_subtotal" type="hidden" value="{{$treatment_planning_design_subtotal}}"/>
                <hr/>
            @endif
            
            @if($key == 'treatment_planning_design_fabrication')
                <h4><strong><u>Treatment Planning (Design & Fabrication)</u></strong></h4>
                @foreach($serviceDetails[$key] as $treatment_planning_design_fabrication)
                    @if(isset($treatment_planning_design_fabrication->bone_reduction_case) && ($treatment_planning_design_fabrication->bone_reduction_case == 1))
                        Bone Reduction Case : {!! display_currency($treatment_planning_design_fabrication_cost['bone_reduction_case']) !!}
                    <?php
                        $total_cost += $treatment_planning_design_fabrication_cost['bone_reduction_case'];
                        $treatment_planning_design_fabrication_subtotal += $treatment_planning_design_fabrication_cost['bone_reduction_case'];
                    ?>
                    @endif
                    @if(isset($treatment_planning_design_fabrication->tooth_supported_case) && ($treatment_planning_design_fabrication->tooth_supported_case == 1))
                        <br/>
                        Tooth Supported Case : {!! display_currency($treatment_planning_design_fabrication_cost['tooth_supported_case']) !!}
                    <?php
                        $total_cost += $treatment_planning_design_fabrication_cost['tooth_supported_case'];
                        $treatment_planning_design_fabrication_subtotal += $treatment_planning_design_fabrication_cost['tooth_supported_case'];
                    ?>
                    @endif
                    @if(isset($treatment_planning_design_fabrication->tissue_supported_case) && ($treatment_planning_design_fabrication->tissue_supported_case == 1))
                        <br/>
                        Tissue Supported Case : {!! display_currency($treatment_planning_design_fabrication_cost['tissue_supported_case']) !!}
                    <?php
                        $total_cost += $treatment_planning_design_fabrication_cost['tissue_supported_case'];
                        $treatment_planning_design_fabrication_subtotal += $treatment_planning_design_fabrication_cost['tissue_supported_case'];
                    ?>
                    @endif
                    @if(isset($treatment_planning_design_fabrication->bone_supported_case) && ($treatment_planning_design_fabrication->bone_supported_case == 1))
                        <br/>
                        Bone Supported Case : {!! display_currency($treatment_planning_design_fabrication_cost['bone_supported_case']) !!}
                    <?php
                        $total_cost += $treatment_planning_design_fabrication_cost['bone_supported_case'];
                        $treatment_planning_design_fabrication_subtotal += $treatment_planning_design_fabrication_cost['bone_supported_case'];
                    ?>
                    @endif
                    @if(isset($treatment_planning_design_fabrication->provisional_planning) && ($treatment_planning_design_fabrication->provisional_planning == 1))
                        <br/>
                        Provisional Planning : {!! display_currency($treatment_planning_design_fabrication_cost['provisional_planning']) !!}
                    <?php
                        $total_cost += $treatment_planning_design_fabrication_cost['provisional_planning'];
                        $treatment_planning_design_fabrication_subtotal += $treatment_planning_design_fabrication_cost['provisional_planning'];
                    ?>
                    @endif
                    @if(isset($treatment_planning_design_fabrication->tooth_supported_case) && ($treatment_planning_design_fabrication->no_of_implants > 1))
                        <br/>
                        <?php $temp_calc = $treatment_planning_design_fabrication_cost['tooth_supported_case_additional'] * (abs($treatment_planning_design_fabrication->no_of_implants) - 1); ?>
                        Additional Implant Cost : {!! display_currency($treatment_planning_design_fabrication_cost['tooth_supported_case_additional']) !!} x  {!! display_currency(abs($treatment_planning_design_fabrication->no_of_implants) - 1) !!} = {!! display_currency($temp_calc) !!}
                    <?php
                        $total_cost += $temp_calc;
                        $treatment_planning_design_fabrication_subtotal += $temp_calc;
                    ?>
                    @endif
                    @if(isset($treatment_planning_design_fabrication->preferred_implant))
                        <br/>
                        Preferred Implant : {{$treatment_planning_design_fabrication->preferred_implant}}
                    @endif
                    @if(isset($treatment_planning_design_fabrication->jaw_information))
                        <br/>
                        Jaw Information : {{$treatment_planning_design_fabrication->jaw_information}}
                    @endif
                    @if(isset($treatment_planning_design_fabrication->restorative_plan))
                        <br/>
                        Restorative Plan : {{$treatment_planning_design_fabrication->restorative_plan}}
                    @endif
                    @if(isset($treatment_planning_design_fabrication->surgery_date))
                        <br/>
                        Surgery Date : {{$treatment_planning_design_fabrication->surgery_date}}
                    @endif
                    @if(isset($treatment_planning_design_fabrication->provisional_planning_reqd))
                        <br/>
                        Provisional Planning Reqd : {{$treatment_planning_design_fabrication->provisional_planning_reqd}}
                    @endif
                @endforeach
                <!--<p>{{$treatment_planning_design_fabrication_subtotal}}</p>-->
                <input type="hidden" name="treatment_planning_design_fabrication_cost" value="{{json_encode($treatment_planning_design_fabrication_cost, true)}}"/>
                <input name="treatment_planning_design_fabrication_subtotal" type="hidden" value="{{$treatment_planning_design_fabrication_subtotal}}"/>
                <hr/>
            @endif
            
            @if($key == 'digital_denture')
                <h4><strong><u>Digital Dentures</u></strong></h4>
                @foreach($serviceDetails[$key] as $digital_denture)
                    @if(isset($digital_denture->print) && ($digital_denture->print == 1))
                        Print : {!! display_currency($digital_denture_cost['print']) !!}
                    <?php
                        $total_cost += $digital_denture_cost['print'];
                        $digital_denture_subtotal += $digital_denture_cost['print'];
                    ?>
                    @endif
                    @if(isset($digital_denture->printing_planning) && ($digital_denture->printing_planning == 1))
                        <br/>
                        Printing Planning : {!! display_currency($digital_denture_cost['printing_planning']) !!}
                    <?php
                        $total_cost += $digital_denture_cost['printing_planning'];
                        $digital_denture_subtotal += $digital_denture_cost['printing_planning'];
                    ?>
                    @endif
                    @if(isset($digital_denture->teeth_purchase_print))
                        <br/>
                        Teeth Purchase Print: {!! display_currency($digital_denture->teeth_purchase_print) !!}
                    @endif
                @endforeach
                <!--<p>{{$digital_denture_subtotal}}</p>-->
                <input type="hidden" name="digital_denture_cost" value="{{json_encode($digital_denture_cost, true)}}"/>
                <input name="digital_denture_subtotal" type="hidden" value="{{$digital_denture_subtotal}}"/>
                <hr/>
            @endif
            
            @if($key == 'orthodontic_treatment')
                <h4><strong><u>Orthodontic Treatment</u></strong></h4>
                @foreach($serviceDetails[$key] as $orthodontic_treatment)
                    @if(isset($orthodontic_treatment->ortho_planning) && ($orthodontic_treatment->ortho_planning == 1))
                        Ortho Planning : {!! display_currency({$orthodontic_treatment_cost['ortho_planning']) !!}
                    <?php
                        $total_cost += $orthodontic_treatment_cost['ortho_planning'];
                        $orthodontic_treatment_subtotal += $orthodontic_treatment_cost['ortho_planning'];
                    ?>
                    @endif
                    @if(isset($orthodontic_treatment->ortho_model_print) && ($orthodontic_treatment->ortho_model_print == 1))
                        <br/>
                        <?php $temp_calc = $orthodontic_treatment_cost['ortho_model_print'] * $orthodontic_treatment->no_of_prints; ?>
                        Ortho Model Print : {!! display_currency($orthodontic_treatment_cost['ortho_model_print']) !!} x {!! display_currency(abs($orthodontic_treatment->no_of_prints)) !!} = {!! display_currency($temp_calc) !!}
                    <?php
                        $total_cost += $temp_calc;
                        $orthodontic_treatment_subtotal += $temp_calc;
                    ?>
                    @endif
                    @if(isset($orthodontic_treatment->order_description))
                        <br/>
                        Order Description: {{$orthodontic_treatment->order_description	}}
                    @endif
                    @if(isset($orthodontic_treatment->jaw_information))
                        <br/>
                        Jaw Information: {{$orthodontic_treatment->jaw_information	}}
                    @endif
                    @if(isset($orthodontic_treatment->aligner_frequency))
                        <br/>
                        Aligner Frequency: {{$orthodontic_treatment->aligner_frequency	}}
                    @endif
                    @if(isset($orthodontic_treatment->anterior_movement))
                        <br/>
                        Anterior Movement: {{$orthodontic_treatment->anterior_movement	}}
                    @endif
                    @if(isset($orthodontic_treatment->chief_complaint))
                        <br/>
                        Chief Complaint: {{$orthodontic_treatment->chief_complaint	}}
                    @endif
                @endforeach
                <!--<p>{{$orthodontic_treatment_subtotal}}</p>-->
                <input type="hidden" name="orthodontic_treatment_cost" value="{{json_encode($orthodontic_treatment_cost, true)}}"/>
                <input name="orthodontic_treatment_subtotal" type="hidden" value="{{$orthodontic_treatment_subtotal}}"/>
                <hr/>
            @endif
            
            @if($key == 'radiology_report')
                <h4><strong><u>Radiology Report</u></strong></h4>
                @foreach($serviceDetails[$key] as $radiology_report)
                    @if(isset($radiology_report->flat_rate) && ($radiology_report->flat_rate == 1))
                        Flat Rate : {!! display_currency($radiology_report_cost['flat_rate']) !!}
                    <?php
                        $total_cost += $radiology_report_cost['flat_rate'];
                        $radiology_report_subtotal += $radiology_report_cost['flat_rate'];
                    ?>
                    @endif
                    @if(isset($radiology_report->study_of_purpose))
                        <br/>
                        Study of Purpose: {{$radiology_report->study_of_purpose	}}
                    @endif
                    @if(isset($radiology_report->job_description))
                        <br/>
                        Job Description: {{$radiology_report->job_description	}}
                    @endif
                    @if(isset($radiology_report->date_of_birth))
                        <br/>
                        Date of Birth: {{$radiology_report->date_of_birth	}}
                    @endif
                @endforeach
                <!--<p>{{$radiology_report_subtotal}}</p>-->
                <input type="hidden" name="radiology_report_cost" value="{{json_encode($radiology_report_cost, true)}}"/>
                <input name="radiology_report_subtotal" type="hidden" value="{{$radiology_report_subtotal}}"/>
                <hr/>
            @endif
            
            @if($key == 'metal_components')
                <h4><strong><u>metal_components</u></strong></h4>
                @foreach($serviceDetails[$key] as $metal_components)
                    @if(isset($metal_components->guide_tubes) && ($metal_components->guide_tubes == 1))
                        Guide Tubes :  {!! display_currency($metal_components_cost['guide_tubes']) !!}
                    <?php
                        $total_cost += $metal_components_cost['guide_tubes'];
                        $metal_components_subtotal += $metal_components_cost['guide_tubes'];
                    ?>
                    @endif
                    @if(isset($metal_components->fixation_pins) && ($metal_components->fixation_pins == 1))
                        <br/>
                        Fixation Pins :  {!! display_currency($metal_components_cost['fixation_pins']) !!}
                    <?php
                        $total_cost += $metal_components_cost['fixation_pins'];
                        $metal_components_subtotal += $metal_components_cost['fixation_pins'];
                    ?>
                    @endif
                @endforeach
                <!--<p>{{$metal_components_subtotal}}</p>-->
                <input type="hidden" name="metal_components_cost" value="{{json_encode($metal_components_cost, true)}}"/>
                <input name="metal_components_subtotal" type="hidden" value="{{$metal_components_subtotal}}"/>
                <hr/>
            @endif
            
            @if($key == 'zygoma_guided_surgery')
                <h4><strong><u>Zygoma Guided Surgery (Design Only)</u></strong></h4>
                @foreach($serviceDetails[$key] as $zygoma_guided_surgery)
                    @if(isset($zygoma_guided_surgery->quadrant_pterygoid) && ($zygoma_guided_surgery->quadrant_pterygoid == 1))
                        Quadrant Pterygoid : {!! display_currency($zygoma_guided_surgery_cost['quadrant_pterygoid']) !!}
                    <?php
                        $total_cost += $zygoma_guided_surgery_cost['quadrant_pterygoid'];
                        $zygoma_guided_surgery_subtotal += $zygoma_guided_surgery_cost['quadrant_pterygoid'];
                    ?>
                    @endif
                    @if(isset($zygoma_guided_surgery->full_pterygoid) && ($zygoma_guided_surgery->full_pterygoid == 1))
                        <br/>
                        Full Pterygoid : {!! display_currency($zygoma_guided_surgery_cost['full_pterygoid']) !!}
                    <?php
                        $total_cost += $zygoma_guided_surgery_cost['full_pterygoid'];
                        $zygoma_guided_surgery_subtotal += $zygoma_guided_surgery_cost['full_pterygoid'];
                    ?>
                    @endif
                @endforeach
                <!--<p>{{$zygoma_guided_surgery_subtotal}}</p>-->
                <input type="hidden" name="zygoma_guided_surgery_cost" value="{{json_encode($zygoma_guided_surgery_cost, true)}}"/>
                <input name="zygoma_guided_surgery_subtotal" type="hidden" value="{{$zygoma_guided_surgery_subtotal}}"/>
                <hr/>
            @endif
            
            @endforeach
            
        <h4>Total: {!! display_currency($total_cost) !!}</h4>
        <input name="serviceDetails" type="hidden" value="{{json_encode($serviceDetails,true)}}"/>
        <input id="total_cost_field" name="total_cost" type="hidden" value="{{$total_cost}}"/>
            <div id="discountBox" style="display:none">
                <p>Discount: (-) $<span id="discountAmount"></span></p>
                <h4>Total Payable: $<span id="payTotal"></span></h4>
            </div>
        </div>
        
        <div class="card-footer text-muted">
        
        <div class="card card-body bg-light">
            <div class="input-group mb-3">
              <input id="coupon_code_field" name="coupon_code" type="text" class="form-control" placeholder="Enter Coupon Code" aria-label="Enter Coupon Code" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button id="couponBtn" class="btn btn-outline-secondary" type="button">Apply</button>
              </div>
            </div>
        </div>
        
            <button id="backBtn" type="button" class="btn btn-primary" onclick="goBack()">Back</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
    
    </form>
    
            </div>
        </div>
    </section>
@endsection

@section('script')
    @parent
<script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
<script type="text/javascript">
function goBack() {
                document.getElementById("backBtn").innnerHTML="Please wait...";
                window.history.back();
                return;
            }

$(function () {
    $(document).on('click','#couponBtn', function(e){
        var $this = $(this);
        var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> Applying...';
        if ($(this).html() !== loadingText) {
          $this.data('original-text', $(this).html());
          $this.html(loadingText);
        }
        /*setTimeout(function() {
          $this.html($this.data('original-text'));
        }, 2000);*/
        
        var coupon_code = $('#coupon_code_field').val();
        var total_amount = $('#total_cost_field').val();
        
        $.ajax({
                		        type: 'POST',
                		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                		        url: '/doc/applycoupon/' + coupon_code + '/' + total_amount,
                		        data: {
                		            services : {
                    		            bone_segmentation : $('input[name="bone_segmentation_subtotal"]').length ? $('input[name="bone_segmentation_subtotal"]').val() : null,
                    		            treatment_planning_design : $('input[name="treatment_planning_design"]').length ? $('input[name="treatment_planning_design"]').val() : null,
                    		            treatment_planning_design_fabrication : $('input[name="treatment_planning_design_fabrication"]').length ? $('input[name="treatment_planning_design_fabrication"]').val() : null,
                    		            digital_denture : $('input[name="digital_denture"]').length ? $('input[name="digital_denture"]').val() : null,
                    		            orthodontic_treatment : $('input[name="orthodontic_treatment"]').length ? $('input[name="orthodontic_treatment"]').val() : null,
                    		            radiology_report : $('input[name="radiology_report"]').length ? $('input[name="radiology_report"]').val() : null,
                    		            metal_components : $('input[name="metal_components"]').length ? $('input[name="metal_components"]').val() : null,
                    		            zygoma_guided_surgery : $('input[name="zygoma_guided_surgery"]').length ? $('input[name="zygoma_guided_surgery"]').val() : null
                		            }
                		        },
                		        success:function(data){
                		            /*console.log(data);*/
                		            if(data && data.discountAmount) {
                		                $('#discountAmount').text(data.discountAmount);
                    		            $('#payTotal').text(data.payTotal);
                    		            $('#discountBox').show();
                		            } else {
                		                $.alert(data.errorMsg[0]);
                		            }
                		            
                		            $this.html($this.data('original-text'));
                		        },
                		        error: function(data){
                		            $this.html($this.data('original-text'));
                		            alert("An error has occurred. Please try again.");
                		        }
                		    });
        
    });
});
</script>
@endsection
