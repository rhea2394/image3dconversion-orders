<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFormColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bone_segmentation', function (Blueprint $table) {
            $table->integer('no_of_implants')->nullable();
            $table->string('preferred_implant')->nullable();
            $table->string('preferred_implant_others')->nullable();
        });
        
        Schema::table('treatment_planning_design', function (Blueprint $table) {
            $table->string('preferred_implant_others')->nullable();
            $table->integer('desired_location_for_implant')->nullable();
            $table->text('preferred_size')->nullable();
            $table->string('increase_ap_spread')->nullable();
            $table->string('immediate_extractions_perform')->nullable();
            $table->string('bone_grafting')->nullable();
            $table->string('bone_reduction')->nullable();
            $table->text('surgical_kit')->nullable();
            $table->string('drill_key')->nullable();
            $table->string('drill_kit_to_use')->nullable();
            $table->string('metal_sleeve_sizes')->nullable();
        });
        
        Schema::table('treatment_planning_design_fabrication', function (Blueprint $table) {
            $table->string('preferred_implant_others')->nullable();
            $table->integer('desired_location_for_implant')->nullable();
            $table->text('preferred_size')->nullable();
            $table->string('increase_ap_spread')->nullable();
            $table->string('immediate_extractions_perform')->nullable();
            $table->string('bone_grafting')->nullable();
            $table->string('bone_reduction')->nullable();
            $table->text('surgical_kit')->nullable();
            $table->string('drill_key')->nullable();
            $table->string('drill_kit_to_use')->nullable();
            $table->string('metal_sleeve_sizes')->nullable();
        });
        
        Schema::table('digital_denture', function (Blueprint $table) {
            $table->text('job_description')->nullable();
            $table->text('tooth_library')->nullable();
        });
        
        Schema::table('radiology_report', function (Blueprint $table) {
            $table->string('scan_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
