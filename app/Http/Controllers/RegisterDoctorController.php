<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterDoctorController extends Controller
{
    //register page
    public function registerDoctorPage()
    {
        return view('doc.register_doctor');
    }
}
