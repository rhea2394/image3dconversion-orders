<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagedocCoupon extends Model
{
    protected $table = 'imagedoc_coupons';
}
