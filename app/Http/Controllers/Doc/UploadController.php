<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use Storage;
use App\Http\Controllers\Doc\NotificationController;
use Auth;

class UploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('imagedocauth');
    }
    
    public function ShowUpload($id) {
        $order = Order::find($id);
        $userfiles = Storage::disk('dropbox')->files('orders/'.$id.'/userfiles');
        $adminfiles = Storage::disk('dropbox')->files('orders/'.$id.'/adminfiles');
        
        $data = [
            'order' => $order,
            'userfiles' => $userfiles,
            'adminfiles' => $adminfiles
            ];
        
        return view('doc.orders.upload')->with($data);
    }
    
    public function upload(Request $request, $id) {
        $this->validate($request, [
            'casefile' => 'required|file',
        ]);
        
        $file = $request->file('casefile');
        
        $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $extension = strtolower($file->getClientOriginalExtension());
        $fileName = time().'_'.clean($originalName).'.'.$extension;
        
        $path = 'orders/'.$id.'/userfiles/';
        Storage::disk('dropbox')->put($path.$fileName, file_get_contents($file->getRealPath()), 'public');
        
        $user = Auth::guard('imagedocuser')->user();
        $userid = $user->id;
        
        /*-------------Admin Notification-----------*/
        (new \App\Http\Controllers\NotificationController)->addToNotification([
            'description' => 'Doctor '.$user->name.' has uploaded a new file "'.$fileName.'" for Order #'.$id,
            'statusclass' => 'info',
            'admin_id' => null,
            'uid' => $id
            ]);
        /*-------------------//---------------------*/
        
        /*----------Doctor Notification-----------*/
        (new NotificationController)->notificationViewAction($userid, $id);
        /*---------------//---------------------*/
        
        $data = [
                'msg' => ['File successfully uploaded'],
                'upload' => 1,
                ];
        
        return redirect()->back()->with($data);
    }
    
    public function downloadFile($path) {
        $path = base64_decode($path);
        // return Storage::disk('dropbox')->download($path);
        $download_url = Storage::disk('dropbox')->url($path);
        return redirect($download_url);
    }
}
