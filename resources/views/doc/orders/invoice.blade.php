<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>i3d invoice</title>
    
    <link href="{{asset('/vendor/adminlte/plugins/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css" media="all" />
    <style>
    body {
        margin:0;
    }
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        /*border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);*/
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr.information td:nth-child(2), .invoice-box table tr.headerinfo td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    /*.invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }*/
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    /*.invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }*/
    
    tr.total td:nth-child(3) {
        text-align: right;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    
    .service_details h4,.service_details p{
        margin:0;
    }
    .price_right {
        float:right;
    }
    </style>
</head>
<?php
$exchange_rate = $order->exchange_rate;
?>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top headerinfo">
                <!--<td colspan="2">
                    <table>
                        <tr class="headerinfo">
                            <td class="title">
                                <img src="{{asset('/images/i3dlogo.png')}}" style="width:100%; max-width:200px;">
                            </td>
                            
                            <td>
                                <h1>Invoice</h1>
                            </td>
                        </tr>
                    </table>
                </td>-->
                <td class="title">
                                <img src="{{asset('/images/i3dlogo.png')}}" style="width:100%; max-width:200px;">
                </td>
                            
                <td>
                                <h1>Invoice</h1>
                </td>
            </tr>
            
            <tr class="information">
                <!--<td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Pub-Sarania Road, Bylane-3<br>
                                H.N:81, 1st Floor, Niva Villa<br>
                                Guwahati, Assam - 781003<br/>
                                India
                            </td>
                            
                            <td>
                                Order: #{{$order->id}}<br>
                                Invoice: #{{invoice($user->id,$order->id)}}<br>
                                Created: {{now_date()}}<br>
                                Due: {{now_date()}}
                            </td>
                        </tr>
                    </table>
                </td>-->
                <td>
                                Pub-Sarania Road, Bylane-3<br>
                                H.N:81, 1st Floor, Niva Villa<br>
                                Guwahati, Assam - 781003<br/>
                                India
                            </td>
                            
                            <td>
                                Order: #{{$order->id}}<br>
                                Invoice: #{{invoice($user->id,$order->id)}}<br>
                                Created: {{now_date()}}<br>
                                Due: {{now_date()}}
                            </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <table cellpadding="0" cellspacing="0">
                        <tr class="heading">
                            <td>
                                Date
                            </td>
                            <td colspan="4">
                                Description
                            </td>
                            <!--<td>
                                Quantity
                            </td>
                            <td>
                                Price
                            </td>
                            <td>
                                Amount
                            </td>-->
                        </tr>
                        <tr class="item">
                            <td>
                                {{$order->created_at->format('d/m/Y')}}
                            </td>
                            <td colspan="4">
                                <div class="service_details">
                                    @include('doc.orders.order_model', ['services' => $services, 'serviceDetails' => $serviceDetails, 'exchange_rate' => $exchange_rate, 'currencyflag' => $order->country])
                                </div>
                            </td>
                            
                        </tr>
                        @if($order->coupon_code)
                            <tr class="total">
                                <td></td>
                                <td colspan="3">
                                    <strong>Total:</strong>
                                </td>
                                <td>
                                   {!! display_currency($order->imagedoc_coupon->total_amount,$order->country,false,$exchange_rate) !!}
                                </td>
                            </tr>
                            <tr class="total">
                                <td></td>
                                <td colspan="3">
                                    <strong>Discount:</strong>
                                </td>
                                <td>
                                   (-) {!! display_currency($order->imagedoc_coupon->discount_amount,$order->country,false,$exchange_rate) !!}
                                </td>
                            </tr>
                            <tr class="total">
                                <td></td>
                                <td colspan="3">
                                    <strong>All Total:</strong>
                                </td>
                                <td>
                                   {!! display_currency($order->imagedoc_coupon->total_payable_amount,$order->country,false,$exchange_rate) !!}
                                </td>
                            </tr>
                        @else
                            <tr class="total">
                                <td></td>
                                <td colspan="3">
                                    <strong>Total:</strong>
                                </td>
                                <td>
                                   {!! display_currency($order->total,$order->country,false,$exchange_rate) !!}
                                </td>
                            </tr>
                        @endif
                        
                        <tr class="total">
                            <td></td>
                            <td colspan="3">
                                <strong>Shipping:</strong>
                            </td>
                            <td>
                               {!! display_currency(0) !!}
                            </td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td colspan="3">
                                <strong>Amount Paid:</strong>
                            </td>
                            <td>
                                @if($order->payment_confirm_flag)
                                <strong>{!! display_currency($order->total,$order->country,false,$exchange_rate) !!}</strong>
                                @else
                                {!! display_currency(0) !!}
                                @endif
                               
                            </td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td colspan="3">
                                <strong>Amount Due:</strong>
                            </td>
                            <td>
                                @if($order->payment_confirm_flag)
                                {!! display_currency(0) !!}
                                @else
                                <strong>{!! display_currency($order->total,$order->country,false,$exchange_rate) !!}</strong>
                                @endif
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <p>{{($order->country == 'india') ? '(Amount inclusive of 18% GST)' : ''}}</p>
        @if($order->extra_addon_service)
        <hr/>
        <p><strong>Extra Addon Service:</strong> {{$order->extra_addon_service}}</p>
        <p><strong>Description:</strong> {!!$order->notes!!}</p>
        <p><strong>Extra Amount:</strong> ${{$order->extra_amount}}</p>
        @endif
    </div>
</body>
</html>