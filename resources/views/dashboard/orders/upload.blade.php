@extends('layouts.master')

@section('page-title', 'Upload Files #'.$order->id)

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/orders">Orders</a></li>
    <li class="breadcrumb-item active">Upload Files</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
<form method="post" action="/order/{{$order->id}}/upload" enctype="multipart/form-data">
    @csrf
    <div class="col-md-4">
  <input type="file" name="casefile">
  <input type="hidden" name="MAX_FILE_SIZE" value="5000000">
  <br/>
  <br/>
  <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>

@if($userfiles || $adminfiles)
<hr/>
    <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Uploaded Files</h3>
                <br/>
                <p>(Click on the files below to download)</p>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <h4>User Files</h4>
                  <ul>
                  @foreach($userfiles as $file)
                     <li><a href="/download/{{base64_encode($file)}}">{{basename($file)}}</a></li>
                  @endforeach
                  </ul>
                  <h4>Admin Files</h4>
                  <ul>
                  @foreach($adminfiles as $file)
                     <li><a href="/download/{{base64_encode($file)}}">{{basename($file)}}</a></li>
                  @endforeach
                  </ul>
                </div>
    </div>
@endif
            
        </div>
    </section>
@endsection