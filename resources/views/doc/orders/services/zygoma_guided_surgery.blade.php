<div class="card">
    <div class="card-header bg-primary text-white">
        Zygoma Guided Surgery (Design Only)
    </div>
    <div class="card-body">
        <div class="form-check">
            <input name="zygoma_guided_surgery[quadrant_pterygoid]" class="form-check-input" type="checkbox" value="1" id="quadrant_pterygoid">
            <label class="form-check-label" for="quadrant_pterygoid">
                Quadrant Pterygoid / Zygo ({!! display_currency(service_cost('zygoma_guided_surgery',$country)->quadrant_pterygoid) !!})
            </label>
        </div>
        <div class="form-check">
            <input name="zygoma_guided_surgery[full_pterygoid]" class="form-check-input" type="checkbox" value="1" id="full_pterygoid">
            <label class="form-check-label" for="full_pterygoid">
                Full Pterygoid / Zygo ({!! display_currency(service_cost('zygoma_guided_surgery',$country)->full_pterygoid) !!})
            </label>
        </div>
    </div>
    <input name="services[zygoma_guided_surgery]" value="1" type="hidden"/>
</div>