@extends('doclayouts.master')

@section('page-title', 'New Order')

@section('style')
    @parent
<style type="text/css">
.h_iframe iframe {
    width:100%;
    height:100%;
}
.h_iframe {
    height: 600px;
    width:100%;
}
</style>
@endsection


@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/doc">Home</a></li>
    <li class="breadcrumb-item active">New Order</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="h_iframe">
            <iframe src="https://image3dconversion.com/order-form2/?id={{Auth::guard('imagedocuser')->user()->id}}" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </section>
@endsection

