<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetalComponentsCost extends Model
{
    protected $table = 'metal_components_cost';
    
    protected $fillable = [
        'country'
    ];
}
