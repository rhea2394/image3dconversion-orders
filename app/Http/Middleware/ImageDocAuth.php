<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class ImageDocAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::guard('imagedocuser')->check()) {
            return redirect('doc/login');
        }

        return $next($request);
    }
}
